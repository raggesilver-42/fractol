<div align="center">
    <h1>
        <img src="https://imgur.com/vqopa7K.png" width="128"/> Fractol
    </h1>
    <h4>A fractal project @ 42</h4>
    <p>
        <a href="https://gitlab.com/raggesilver-42/fractol/pipelines">
            <img src="https://gitlab.com/raggesilver-42/fractol/badges/master/pipeline.svg" alt="Build Status" />
        </a>
        <a href="https://www.patreon.com/raggesilver">
            <img src="https://img.shields.io/badge/patreon-donate-orange.svg?logo=patreon" alt="Patreon" />
        </a>
    </p>
    <p>
        <a href="#install">Install</a> •
        <a href="#features">Features</a> •
        <a href="#requirements">Requirements</a>
    </p>
</div>

> Write description

## Install

> Write install instructions

## Features
> Write feature list

## Requirements

The required libraries are submodules of this project. Just clone with
`--recursive` and you'll be good (assuming you have `make` and `gcc >= 8`).

> If you use another distro and would like me to add the package list here just
> open an issue and I'll look into it.
