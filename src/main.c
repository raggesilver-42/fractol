/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/01 00:16:41 by pqueiroz          #+#    #+#             */
/*   Updated: 2020/02/10 13:39:24 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol_priv.h"

static const char	*g_fracts[5] = {
	"julia",
	"burning ship",
	"mandelbrot",
	"tricorn",
	NULL,
};

int
	main(int argc, const char **argv)
{
	t_fractol	fractol;
	size_t		fract;

	if (argc == 2)
	{
		if (!ft_strv_get_index_of((char **)g_fracts, argv[1], &fract))
			return (ft_dprintf(2, "ft_fractol: Invalid fract '%s'\n", argv[1]));
		fol_new(&fractol, fract);
		fol_connect_events(&fractol);
		return (mlx_loop(fractol.mlx));
	}
	else
	{
		ft_dprintf(2, "ft_fractol: usage: ./ft_fractol <fract name>\n");
		return (1);
	}
}
