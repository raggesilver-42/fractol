/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_priv.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/03 18:18:35 by pqueiroz          #+#    #+#             */
/*   Updated: 2020/02/10 12:44:00 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SET_PRIV_H
# define SET_PRIV_H

# include "../fractol_priv.h"
# include <math.h>

# define COLOR_BASE 0x110613

typedef struct	s_set_class
{
	t_fol_set	parent;
	t_fractol	*f;
	int			iterations;
	int			color_base;
	double		c_re;
	double		c_im;
	double		n_re;
	double		n_im;
	double		o_re;
	double		o_im;
	double		pr;
	double		pi;
	double		x;
	double		y;
}				t_set_class;

t_set_class		*set_class_get_priv(t_fol_set *ptr);
int				fol_get_color(t_set_class *self, int i);

#endif
