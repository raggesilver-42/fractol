/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   julia.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/03 15:44:31 by pqueiroz          #+#    #+#             */
/*   Updated: 2020/02/10 15:15:01 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "set_priv.h"

static void
	julia_init(t_fol_set *self, t_fractol *fractol)
{
	fol_hook_iterations(fractol, &set_class_get_priv(self)->iterations);
}

static int
	julia_calc(t_set_class *self)
{
	int			i;
	t_set_class	*s;

	i = -1;
	s = self;
	while (++i < s->iterations)
	{
		s->o_re = s->n_re;
		s->o_im = s->n_im;
		s->n_re = (s->o_re * s->o_re) - s->o_im * s->o_im + s->c_re;
		s->n_im = 2 * s->o_re * s->o_im + s->c_im;
		if ((s->n_re * s->n_re + s->n_im * s->n_im) > 4)
			break ;
	}
	return (i);
}

static int
	julia_update(t_fol_set *set, t_fractol *f)
{
	t_set_class	*const	self = set_class_get_priv(set);
	int					color;
	int					x;
	int					y;
	int					i;

	y = -1;
	while (++y < WINDOW_HEIGHT)
	{
		x = -1;
		while (++x < WINDOW_WIDTH)
		{
			self->n_re = 1.5 * (x - WINDOW_WIDTH / 2) /
				(0.5 * f->zoom * WINDOW_WIDTH) + f->camera.x;
			self->n_im = (y - WINDOW_HEIGHT / 2) /
				(0.5 * f->zoom * WINDOW_HEIGHT) + f->camera.y;
			i = julia_calc(self);
			color = fol_get_color(self, i);
			f->img.data[y * WINDOW_WIDTH + x] = color;
		}
	}
	mlx_put_image_to_window(f->mlx, f->window, f->img.ptr, 0, 0);
	return (0);
}

static void
	julia_class_init(t_set_class *self)
{
	self->c_re = -0.7;
	self->iterations = 150;
	self->c_im = 0.3;
	self->color_base = COLOR_BASE;
}

t_fol_set
	*fol_get_julia(t_fractol *fractol)
{
	static t_set_class	self;
	static int			set = FALSE;
	t_fol_set			*res;

	if (set == FALSE)
	{
		set = TRUE;
		ft_memset(&self, 0, sizeof(self));
		res = &self.parent;
		res->init = &julia_init;
		res->update = &julia_update;
		res->name = "Fractol - Julia";
		self.f = fractol;
		julia_class_init(&self);
	}
	return ((t_fol_set *)&self);
}
