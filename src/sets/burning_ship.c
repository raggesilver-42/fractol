/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   burning_ship.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/03 15:52:18 by pqueiroz          #+#    #+#             */
/*   Updated: 2020/02/10 14:48:44 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "set_priv.h"

static void
	burning_ship_init(t_fol_set *self, t_fractol *fractol)
{
	fol_hook_iterations(fractol, &set_class_get_priv(self)->iterations);
}

static int
	burning_ship_calc(t_set_class *self)
{
	int			i;
	t_set_class	*s;

	i = -1;
	s = self;
	while (++i < s->iterations)
	{
		s->o_re = s->n_re;
		s->o_im = s->n_im;
		s->n_re = fabs(s->o_re * s->o_re - s->o_im * s->o_im + s->x);
		s->n_im = fabs(2 * s->o_re * s->o_im + s->y);
		if ((s->n_re * s->n_re + s->n_im * s->n_im) > 4)
			break ;
	}
	return (i);
}

static int
	burning_ship_update(t_fol_set *set, t_fractol *f)
{
	t_set_class	*const	self = set_class_get_priv(set);
	int					color;
	int					x;
	int					y;
	int					i;

	y = -1;
	while (++y < WINDOW_HEIGHT)
	{
		x = -1;
		while (++x < WINDOW_WIDTH)
		{
			self->x = 1.5 * (x - WINDOW_WIDTH / 2) /
				(0.5 * f->zoom * WINDOW_WIDTH) + f->camera.x;
			self->n_re = self->x;
			self->y = (y - WINDOW_HEIGHT / 2) /
				(0.5 * f->zoom * WINDOW_HEIGHT) + f->camera.y;
			self->n_im = self->y;
			i = burning_ship_calc(self);
			color = fol_get_color(self, i);
			f->img.data[y * WINDOW_WIDTH + x] = color;
		}
	}
	mlx_put_image_to_window(f->mlx, f->window, f->img.ptr, 0, 0);
	return (0);
}

static void
	burning_ship_class_init(t_set_class *self)
{
	self->iterations = 200;
	self->color_base = COLOR_BASE;
}

t_fol_set
	*fol_get_burning_ship(t_fractol *fractol)
{
	static t_set_class	self;
	static int			set = FALSE;
	t_fol_set			*res;

	(void)fractol;
	if (set == FALSE)
	{
		set = TRUE;
		ft_memset(&self, 0, sizeof(self));
		res = &self.parent;
		res->init = &burning_ship_init;
		res->update = &burning_ship_update;
		res->name = "Fractol - Burning Ship";
		self.f = fractol;
		burning_ship_class_init(&self);
	}
	return ((t_fol_set *)&self);
}
