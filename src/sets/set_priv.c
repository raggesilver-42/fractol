/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_priv.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/05 14:17:10 by pqueiroz          #+#    #+#             */
/*   Updated: 2020/02/10 12:44:44 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "set_priv.h"

inline t_set_class
	*set_class_get_priv(t_fol_set *ptr)
{
	return ((t_set_class *)ptr);
}

inline int
	fol_get_color(t_set_class *self, int i)
{
	int r;

	r = i % self->iterations * (i < self->iterations);
	return ((r << 21) | (r << 10) | r);
}
