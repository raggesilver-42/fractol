/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mandelbrot.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/03 15:52:08 by pqueiroz          #+#    #+#             */
/*   Updated: 2020/02/10 14:48:47 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "set_priv.h"

static void
	mandelbrot_init(t_fol_set *self, t_fractol *fractol)
{
	fol_hook_iterations(fractol, &set_class_get_priv(self)->iterations);
}

static int
	mandelbrot_calc(t_set_class *self)
{
	int			i;
	t_set_class	*s;

	i = -1;
	s = self;
	while (++i < s->iterations)
	{
		s->o_re = s->n_re;
		s->o_im = s->n_im;
		s->n_re = s->o_re * s->o_re - s->o_im * s->o_im + s->pr;
		s->n_im = 2 * s->o_re * s->o_im + s->pi;
		if ((s->n_re * s->n_re + s->n_im * s->n_im) > 4)
			break ;
	}
	return (i);
}

static int
	mandelbrot_update(t_fol_set *set, t_fractol *f)
{
	t_set_class	*const	self = set_class_get_priv(set);
	int					color;
	int					x;
	int					y;

	y = -1;
	while (++y < WINDOW_HEIGHT)
	{
		x = -1;
		while (++x < WINDOW_WIDTH)
		{
			self->pr = 1.5 * (x - WINDOW_WIDTH / 2) /
				(0.5 * f->zoom * WINDOW_WIDTH) + f->camera.x;
			self->pi = (y - WINDOW_HEIGHT / 2) /
				(0.5 * f->zoom * WINDOW_HEIGHT) + f->camera.y;
			self->o_re = 0;
			self->n_re = 0;
			self->o_im = 0;
			self->n_im = 0;
			color = fol_get_color(self, mandelbrot_calc(self));
			f->img.data[y * WINDOW_WIDTH + x] = color;
		}
	}
	mlx_put_image_to_window(f->mlx, f->window, f->img.ptr, 0, 0);
	return (0);
}

static void
	mandelbrot_class_init(t_set_class *self)
{
	self->iterations = 150;
	self->color_base = COLOR_BASE;
}

t_fol_set
	*fol_get_mandelbrot(t_fractol *fractol)
{
	static t_set_class	self;
	static int			set = FALSE;
	t_fol_set			*res;

	(void)fractol;
	if (set == FALSE)
	{
		set = TRUE;
		ft_memset(&self, 0, sizeof(self));
		res = &self.parent;
		res->init = &mandelbrot_init;
		res->update = &mandelbrot_update;
		res->name = "Fractol - Mandelbrot";
		self.f = fractol;
		mandelbrot_class_init(&self);
	}
	return ((t_fol_set *)&self);
}
