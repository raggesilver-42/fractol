/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   aux.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/03 14:48:31 by pqueiroz          #+#    #+#             */
/*   Updated: 2020/02/04 15:01:22 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol_priv.h"

#if IS_LINUX
# include "mlx_int.h"
# include <X11/X.h>
# define CLOSE_DISPLAY	XCloseDisplay
# define T_XVAR			t_xvar
#else
# define CLOSE_DISPLAY	void
# define T_XVAR			t_mock_xvar
#endif

static t_fractol	*g_fractol = NULL;

/*
** Functions to initialize and free t_fractol
*/

static void __attribute__((destructor))
	fk_on_exit(void)
{
	if (g_fractol && g_fractol->mlx)
	{
		mlx_clear_window(g_fractol->mlx, g_fractol->window);
		mlx_destroy_window(g_fractol->mlx, g_fractol->window);
		(CLOSE_DISPLAY)((void *)((T_XVAR *)g_fractol->mlx)->display);
		free(g_fractol->mlx);
		g_fractol->mlx = NULL;
	}
}

int __attribute__((noreturn))
	fk_on_client_message(t_fractol *self)
{
	(void)self;
	fk_on_exit();
	exit(0);
}

#if IS_LINUX

/*
** The linux fk_init uses XSetWMProtocols to receive the destroy signal and
** properly exit fractol once the user hits the close button.
*/

static void
	fk_init(t_fractol *self)
{
	Atom		wm_delete_window;
	t_win_list	*w;
	t_xvar		*xvar;

	self->mlx = mlx_init();
	self->window = mlx_new_window(
		self->mlx, WINDOW_WIDTH, WINDOW_HEIGHT, "Fractol");
	w = self->window;
	xvar = self->mlx;
	wm_delete_window = XInternAtom(xvar->display, "WM_DELETE_WINDOW", 0);
	XSetWMProtocols(xvar->display, w->window, &wm_delete_window, 1);
	mlx_hook(self->window, ClientMessage, 0, &fk_on_client_message, self);
}

#else

static void
	fk_init(t_fractol *self)
{
	self->mlx = mlx_init();
	self->window = mlx_new_window(
		self->mlx, WINDOW_WIDTH, WINDOW_HEIGHT, "Fractol");
}

#endif

void
	fol_new(t_fractol *out, size_t fract)
{
	int	n;

	ft_memset(out, 0, sizeof(*out));
	out->zoom = 1;
	out->changed = TRUE;
	out->close = &fk_on_client_message;
	out->fract = (1 << fract);
	g_fractol = out;
	fk_init(out);
	out->img.ptr = mlx_new_image(out->mlx, WINDOW_WIDTH, WINDOW_HEIGHT);
	out->img.data = (t_uint32 *)mlx_get_data_addr(out->img.ptr, &n, &n, &n);
	fol_init_set(out);
}
