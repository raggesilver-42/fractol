/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   events.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/01 11:47:09 by pqueiroz          #+#    #+#             */
/*   Updated: 2020/02/10 15:21:06 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol_priv.h"

static void
	fk_move_camera(int key, t_fractol *self)
{
	if (key == FOL_KEY_RIGHT)
		self->camera.x += 0.1 / self->zoom;
	else if (key == FOL_KEY_LEFT)
		self->camera.x -= 0.1 / self->zoom;
	else if (key == FOL_KEY_UP)
		self->camera.y -= 0.1 / self->zoom;
	else if (key == FOL_KEY_DOWN)
		self->camera.y += 0.1 / self->zoom;
	else if (key == FOL_KEY_EQUALS)
		self->zoom *= 1.5;
	else if (key == FOL_KEY_MINUS)
		self->zoom /= 1.5;
	else
		return ;
	self->changed = TRUE;
}

static int
	fk_on_key(int key, t_fractol *self)
{
	if (key == FOL_KEY_ESCAPE)
		self->close();
	else if (key == FOL_KEY_LEFT || key == FOL_KEY_RIGHT ||
		key == FOL_KEY_UP || key == FOL_KEY_DOWN ||
		key == FOL_KEY_MINUS || key == FOL_KEY_EQUALS)
		fk_move_camera(key, self);
	else if (key == FOL_KEY_J && self->iterations)
		*self->iterations -= (*self->iterations >= 25) ? 25 : 0;
	else if (key == FOL_KEY_K && self->iterations)
		*self->iterations += 25;
	else
	{
		ft_printf("Key %05d\n", key);
		return (0);
	}
	self->changed = TRUE;
	return (0);
}

static int
	fk_on_button_press(int key, int x, int y, t_fractol *self)
{
	(void)x;
	(void)y;
	if (key == FOL_BUTTON_SCROLL_UP)
		fk_move_camera(FOL_KEY_EQUALS, self);
	else if (key == FOL_BUTTON_SCROLL_DOWN)
		fk_move_camera(FOL_KEY_MINUS, self);
	else
		return (0);
	self->changed = TRUE;
	return (0);
}

void
	fol_connect_events(t_fractol *self)
{
	mlx_hook(self->window, FOL_EVENT_KEY_PRESS, FOL_MASK_KEY_PRESS,
				&fk_on_key, self);
	mlx_hook(self->window, FOL_EVENT_BUTTON_PRESS, FOL_MASK_BUTTON_PRESS,
				&fk_on_button_press, self);
	mlx_do_key_autorepeaton(self->mlx);
}
