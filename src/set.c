/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/03 14:17:37 by pqueiroz          #+#    #+#             */
/*   Updated: 2020/02/10 15:21:29 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol_priv.h"

static int
	fk_set_hook(t_fractol *self)
{
	if (self->changed)
	{
		self->changed = FALSE;
		return (self->set->update(self->set, self));
	}
	return (0);
}

void
	fol_hook_iterations(t_fractol *self, int *iterations)
{
	self->iterations = iterations;
}

void
	fol_init_set(t_fractol *self)
{
	if (self->fract == FRACT_JULIA)
		self->set = fol_get_julia(self);
	else if (self->fract == FRACT_MANDELBROT)
		self->set = fol_get_mandelbrot(self);
	else if (self->fract == FRACT_TRICORN)
		self->set = fol_get_tricorn(self);
	else if (self->fract == FRACT_BURNING_SHIP)
		self->set = fol_get_burning_ship(self);
	self->set->init(self->set, self);
	mlx_loop_hook(self->mlx, &fk_set_hook, self);
}
