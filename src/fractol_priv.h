/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol_priv.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/01 00:18:05 by pqueiroz          #+#    #+#             */
/*   Updated: 2020/02/10 14:56:13 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_PRIV_H
# define FRACTOL_PRIV_H

# include "libft.h"
# include "mlx.h"

# if defined(__linux__) && __linux__
#  define IS_LINUX TRUE
# else
#  define IS_LINUX FALSE
# endif

/*
** Window dimentions ===========================================================
**
** You may compile with `CFLAGS="-DWINDOW_XXXX=YYYY ..." make -j` to specify
** different window dimentions
*/

# ifndef WINDOW_WIDTH
#  define WINDOW_WIDTH 640
# endif

# ifndef WINDOW_HEIGHT
#  define WINDOW_HEIGHT 480
# endif

/*
** Crossplatform keycode definitions ===========================================
*/

# if IS_LINUX
#  include <X11/keysym.h>
#  define FOL_KEY_ESCAPE	XK_Escape
#  define FOL_KEY_LEFT		65361
#  define FOL_KEY_UP		65362
#  define FOL_KEY_RIGHT		65363
#  define FOL_KEY_DOWN		65364
#  define FOL_KEY_MINUS		45
#  define FOL_KEY_EQUALS	61
#  define FOL_KEY_J			XK_j
#  define FOL_KEY_K			XK_k
# else
#  define FOL_KEY_ESCAPE 	53
#  define FOL_KEY_LEFT		123
#  define FOL_KEY_UP		126
#  define FOL_KEY_RIGHT		124
#  define FOL_KEY_DOWN		125
#  define FOL_KEY_MINUS		27
#  define FOL_KEY_EQUALS	24
#  define FOL_KEY_J			38
#  define FOL_KEY_K			40
# endif

/*
** (Mouse) Buttons
*/

# define FOL_BUTTON_SCROLL_UP		4
# define FOL_BUTTON_SCROLL_DOWN		5

/*
** Events
*/

# define FOL_EVENT_KEY_PRESS		2
# define FOL_EVENT_BUTTON_PRESS		4
# define FOL_EVENT_MOUSE_MOVE		6

/*
** Masks
*/

# define FOL_MASK_KEY_PRESS			1
# define FOL_MASK_BUTTON_PRESS		4
# define FOL_MASK_POINTER_MOTION	64

typedef struct		s_point
{
	double			x;
	double			y;
}					t_point;

enum				e_fract
{
	FRACT_JULIA = 1 << 0,
	FRACT_BURNING_SHIP = 1 << 1,
	FRACT_MANDELBROT = 1 << 2,
	FRACT_TRICORN = 1 << 3,
	FRACT_INVALID = 1 << 4,
};

/*
** Forward declaration =========================================================
*/

struct s_fractol;

typedef struct		s_fol_set
{
	void			(*init)(struct s_fol_set *, struct s_fractol *);
	int				(*update)(struct s_fol_set *, struct s_fractol *);
	const char		*name;
}					t_fol_set;

typedef struct		s_fractol
{
	void			*mlx;
	void			*window;
	int				(*close)();
	size_t			fract;
	t_point			camera;
	double			zoom;
	t_fol_set		*set;

	int				changed;
	int				*iterations;

	struct			s_image
	{
		void		*ptr;
		t_uint32	*data;
	}				img;
}					t_fractol;

/*
** This is just a compatibility hack for the XCloseDisplay function call in
** fk_on_exit in main.c
*/

typedef struct		s_mock_xvar
{
	void			*display;
}					t_mock_xvar;

void				fol_connect_events(t_fractol *self);

void				fol_new(t_fractol *out, size_t fract);
void				fol_init_set(t_fractol *self);

t_fol_set			*fol_get_julia(t_fractol *self);
t_fol_set			*fol_get_mandelbrot(t_fractol *self);
t_fol_set			*fol_get_burning_ship(t_fractol *self);
t_fol_set			*fol_get_tricorn(t_fractol *self);

/*
** Putting iterations in t_set_class was a bad decision. That makes this
** function necessary. (defined in src/set.c)
*/

void				fol_hook_iterations(t_fractol *self, int *iterations);

#endif
